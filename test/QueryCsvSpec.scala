import org.scalatest.{FlatSpec, Matchers}
import models.{LoadCsvData, QueryCsvOption, Country}

class QueryCsvSpec extends FlatSpec with Matchers {
  val csvModel = new LoadCsvData()
  val queryOption = new QueryCsvOption(csvModel)
  import queryOption._

  //testing country queries

  """number of countries being named "United States" """ should "be 2" in {
    findCountryByName("United States") should have length 2
  }

  """number of countries being named "Mauritius" """ should "be 1" in {
    findCountryByName("Mauritius") should have length 1
  }

  """number of countries being named "MauRiTius " -  case insensitive tested""" should "be 1" in {
    findCountryByName("MauRiTius") should have length 1
  }

  """number of countries containing "United"  word""" should "be 4" in {
    findCountryByName("United") should have length 4
  }

  """countries containing "United"  word """ should """ be  "United Arab Emirates", "United Kingdom", "United States",  "United States Minor Outlying Islands" """ in {
    findCountryByName("United").map(country => country.name) should contain theSameElementsAs
      List("United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands")
  }

  """number of countries starting with "An"  word""" should "be 5" in {
    findCountryStartingWithName("An") should have length 5
  }

  """ countries starting with "An"  word""" should """be "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda" """ in {
    findCountryStartingWithName("An").map(country => country.name) should contain theSameElementsAs
      List("Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda")
  }

  "country list" should """have a country with code "US" """ in {
    findCountryByExactCode("US") shouldBe Some(Country("302755", "US", "United States", "NA", "http://en.wikipedia.org/wiki/United_States", "America"))
  }

  "country list" should """have only one country with code "MU" """ in {
    findCountryBySomeCode("Mu") should have length 1
  }

  "USA have " should "have 21501 airports " in {
    val someCountry = findCountryByExactCode("US")
    val airports = someCountry.map { country => findAirportByCountry(country).length }
    airports shouldBe Some(21501)
  }

  "the total number of  Airports " should "be 46505" in {
    airportsByCountryName("") should have length 46505
  }

  """airport with code "01MN" """ should "have  2 runways" in {
    findRunWaysByAirportIdentifier("01MN") should have length 2
  }

  "Mauritius" should "have  only 3 airports" in {
    val country = findCountryByName("Mauritius")
    val airports = country.flatMap { country => findAirportByCountry(country) }
    airports should have length 3
  }

  "Mauritius" should "have  only 3 airports and 2 runways, first 2 airports have runways and not the last" in {
    val country = findCountryByName("Mauritius")
    val results = findAirportsAndRunwaysByCountryName("Mauritius")
    val airportsNrunways = results.head._2
    airportsNrunways should have length 3
    airportsNrunways(0)._2 should have length 1
    airportsNrunways(1)._2 should have length 1
    airportsNrunways(2)._2 should have length 0
  }

}
