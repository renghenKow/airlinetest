import org.scalatest.FlatSpec
import models.LoadCsvData
import cats._

class  ModelsCsvSpec extends FlatSpec {
  val  csvModel : LoadCsvData = new LoadCsvData()
  val countriesFromGivenFile = csvModel.loadedCountries

  //testing countries

  "number of countries from countries.csv file" should  "have 247 rows" in {
    assert(countriesFromGivenFile.length === 247)
  }

  "number of continents from countries.csv file" should  "be 7 " in {
    assert(countriesFromGivenFile.groupBy(country => country.continent).keys.toList.length   === 7)
  }

  "number of countries in continent 'OC'  from countries.csv file" should  "have 26 rows" in {
    assert(countriesFromGivenFile.filter { country => country.continent === "OC" } .length  ===  26)
  }

  //testing airports

  val airportsFromGivenFile = csvModel.loadedAirports

  "number of rows from airport.csv file" should  "be  46505" in {
    assert(airportsFromGivenFile.length === 46505)
  }

  "number of airports from airport.csv file" should  "be 46505" in {
    assert(airportsFromGivenFile.groupBy(airport  => airport.id).keys.toList.length === 46505)
  }

  "number of airport types from airport.csv file" should  "be 7" in {
    assert(airportsFromGivenFile.groupBy(airport  => airport. typeOf) .keys.toList.length ===  7)
  }

   "number of continents from airport.csv file" should  "be 7" in {
    assert(airportsFromGivenFile.groupBy(airport  => airport. continent) .keys.toList.length ===  7)
   }

    "number of countries from airport.csv file" should  "be less than 246,as unknown country will not be accounted" in {
      assert(airportsFromGivenFile.groupBy(airport  => airport.isoCountry) .keys.toList.length <  246)
    }

  //testing runways

    val  runWaysFromGivenFile = csvModel.loadedRunways

    "number of  runways from runways.csv file" should  "have 39536 rows" in {
    assert(runWaysFromGivenFile.length === 39536)
    }
  
   "number of  Airport references from runways.csv file" should  "be less than 39536" in {
     assert(runWaysFromGivenFile.groupBy(runways => runways.airportRef ).keys.toList.length < 39536)
   }
}
