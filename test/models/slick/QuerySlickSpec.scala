package models.slick
import org.scalatest._
import org.scalatest.time._
import org.scalatestplus.play._
import org.scalatest.concurrent.ScalaFutures

import play.api.libs.concurrent.Execution.Implicits.defaultContext

import models.{Country}
import testhelpers.Injector
import Matchers._
import scala.concurrent.Future

class QuerySlickSpec extends PlaySpec with OneAppPerTest with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(3, Seconds), interval = Span(50, Millis))

  val querySlick = Injector.inject[QuerySlick]
  import querySlick._
  import dbConfig.driver.api._

  //testing country queries

  """number of countries being named "United States" """ should {
    "be 2" in {
      val resultFuture = findCountryByName("United States")
      whenReady(resultFuture) { result =>
        result should have length 2
      }
    }
  }

  """number of countries being named "Mauritius" """ should {
    "be 1" in {
      val resultFuture = findCountryByName("Mauritius")
      whenReady(resultFuture) { result =>
        result should have length 1
      }
    }
  }

  """number of countries being named "MauRiTius " -  case insensitive tested""" should {
    "be 1" in {
      val resultFuture = findCountryByName("MauRiTius")
      whenReady(resultFuture) { result =>
        result should have length 1
      }
    }
  }

  """number of countries containing "United"  word""" should {
    "be 4" in {
      val resultFuture = findCountryByName("United")
      whenReady(resultFuture) { result =>
        result should have length 4
      }
    }
  }

  """countries containing "United"  word """ should {
    """ be  "United Arab Emirates", "United Kingdom", "United States",  "United States Minor Outlying Islands" """ in {
      val resultFuture = findCountryByName("United")
      whenReady(resultFuture) { result =>
        result.map(country => country.name) should contain theSameElementsAs
          List("United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands")
      }
    }
  }

  """number of countries starting with "An"  word""" should {
    "be 5" in {
      val resultFuture = findCountryStartingWithName("An")
      whenReady(resultFuture) { result =>
        result should have length 5
      }
    }
  }

  """ countries starting with "An"  word""" should {
    """be "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda" """ in {
      val resultFuture = findCountryStartingWithName("An")
      whenReady(resultFuture) { result =>
        result.map(country => country.name) should contain theSameElementsAs
          List("Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda")
      }
    }
  }

  "country db list" should {
    """have a country with code "US" """ in {
      val resultFuture = findCountryByExactCode("US")
      whenReady(resultFuture) { result =>
        result shouldBe Vector(Country("302755", "US", "United States", "NA", "http://en.wikipedia.org/wiki/United_States", "America"))
      }
    }
  }

  "country db list" should {
    """have only one country with code "MU" """ in {
      val resultFuture = findCountryBySomeCode("Mu")
      whenReady(resultFuture) { result =>
        result should have length 1
      }
    }
  }

  "USA have " should {
    "have 21501 airports " in {
      val someCountry = findCountryByExactCode("US")
      val resultFuture = someCountry.flatMap { countries => Future.sequence(countries.map { country => findAirportByCountry(country).map(_.length) }) }
      whenReady(resultFuture) { result =>
        result shouldBe Vector(21501)
      }
    }
  }

  "the total number of  Airports " should {
    "be 46505" in {
      val resultFuture = airportsByCountryName("")
      whenReady(resultFuture) { result =>
        result should have length 46505
      }
    }
  }

  """airport with code "01MN" """ should {
    "have  2 runways" in {
      val resultFuture = findRunWaysByAirportIdentifier("01MN")
      whenReady(resultFuture) { result =>
        result should have length 2
      }
    }
  }

  "Mauritius " should {
    "have  only 3 airports" in {

      val countryFuture = findCountryByName("Mauritius")
      val airportsFuture = countryFuture.flatMap { countries =>
        val airports = Future.sequence(countries.map { country => findAirportByCountry(country) })
        airports.map { airport => airport.flatten.toList }
      }

      whenReady(airportsFuture) { result =>
        result should have length 3
      }
    }
  }

  "Mauritius" should {
    "have  only 3 airports and 2 runways, first 2 airports have runways and not the last" in {
      val countriesFuture = findCountryByName("Mauritius")
      val airportsFuture = countriesFuture.flatMap { countries =>
        Future.sequence(countries.map { country => findAirportsAndRunwaysByCountryCode(country.code) })
          .map { _.flatten.toList }
      }
      whenReady(airportsFuture) { results =>
        val airportsNrunways = results.head._2
        airportsNrunways should have length 3
        airportsNrunways(0)._2 should have length 1
        airportsNrunways(1)._2 should have length 1
        airportsNrunways(2)._2 should have length 0
      }
    }
  }

}

