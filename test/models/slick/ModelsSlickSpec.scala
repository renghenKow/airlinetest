package models.slick
import org.scalatest._
import org.scalatest.time._
import org.scalatestplus.play._
import org.scalatest.concurrent.ScalaFutures

import play.api.libs.concurrent.Execution.Implicits.defaultContext

import testhelpers.Injector

class ModelsSlickSpec extends PlaySpec with OneAppPerTest with ScalaFutures {

  implicit  val defaultPatience =
  PatienceConfig(timeout = Span(500, Millis), interval = Span(50, Millis))

  val daos = Injector.inject[DAOs]
  import daos._
  import dbConfig.driver.api._
  //testing countries  

  "number of countries from db" should {
    "have 247 rows" in {
      val query = countries.length
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result === 247)
      }
    }
  }

  "number of continents from countries DB" should {
    "be 7 " in {
      val query = countries.groupBy(country => country.continent).map { case (name, c) => name }
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { continents =>
        assert(continents.length === 7)
      }
    }
  }

  "number of countries in continent 'OC'  from countries DB" should {
    "be 26 " in {
      val query = countries.filter(country => country.continent === "OC")
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result.length === 26)
      }
    }
  }

  //testing airports

  "number of rows from airport db" should {
    "be  46505" in {
      val query = airports.length
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result === 46505)
      }
    }
  }

  "number of airports from airport db" should {
    "be 46505" in {
      val query = airports.groupBy { airport => airport.id }.map { case (id, airport) => id }
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result.length === 46505)
      }
    }
  }

  "number of airport types from airport db" should {
    "be 7" in {
      val query = airports.groupBy { airport => airport.typeOf }.map { case (typeof, airport) => typeof }
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result.length === 7)
      }
    }
  }

  "number of continents from airport db" should {
    "be 7" in {
      val query = airports.groupBy { airport => airport.continent }.map { case (con, airport) => con }
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result.length === 7)
      }
    }
  }

  "number of countries from airport db" should {
    "be less than 246,as unknown country will not be accounted" in {
      val query = airports.groupBy { airport => airport.isoCountry }.map { case (isoCountry, airport) => isoCountry }
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result.length < 246)
      }
    }
  }

   //testing runways

  "number of  runways from runways db" should{
    "have 39536 rows" in {
      val query = runways.length
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result === 39536)
      }
    }
  }
  
  "number of  Airport references from runways db" should {
    "be less than 39536" in {
        val query = runways.groupBy { runway => runway.airportRef }.map { case ( airportRef,runway) => airportRef }
      val resultFuture = db.run(query.result)
      whenReady(resultFuture) { result =>
        assert(result.length < 39536)
      }
    }
  }




}
