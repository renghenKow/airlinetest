import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import play.api.ApplicationLoader.Context

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ApplicationSlickSpec extends PlaySpec with OneAppPerTest {
  "Routes" should {

    "send 200 on a /slick/ request" in {       
      route(app, FakeRequest(GET, "/slick/")).map(status(_)) mustBe Some(OK)
    }

     "send 200 on a /reporting request" in {       
      route(app, FakeRequest(GET, "/slick/reporting")).map(status(_)) mustBe Some(OK)
     }

    "send 200 on a /slick/queryByCode request" in {
      val data = Map("code" -> "ZW")
      val fakeRequest =  FakeRequest(POST, "/queryByCode",FakeHeaders(),"")
      route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).map(status(_)) mustBe Some(OK)
      //val fakeRequest = FakeRequest(Helpers.POST, controllers.routes.ApplicationController.createGroup().url, FakeHeaders(), """ {"name": "New Group", "collabs": ["foo", "asdf"]} """)

    }

    "send 400 on a /slick/queryByCodeRest request" in {
      val data = Map("fake" -> "ZW")
      val fakeRequest =  FakeRequest(POST, "/queryByCodeRest",FakeHeaders(),"")
      route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).map(status(_)) mustBe Some(BAD_REQUEST)
    }

     "send 200 on a /slick/queryByCodeRest request" in {
      val data = Map("code" -> "ZW")
      val fakeRequest =  FakeRequest(POST, "/queryByCodeRest",FakeHeaders(),"")
      route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).map(status(_)) mustBe Some(OK)
      //val fakeRequest = FakeRequest(Helpers.POST, controllers.routes.ApplicationController.createGroup().url, FakeHeaders(), """ {"name": "New Group", "collabs": ["foo", "asdf"]} """)

     }

    //query by name test

    "send 200 on a /slick/queryByName request" in {
      val data = Map("name" -> "zimb")
      val fakeRequest =  FakeRequest(POST, "/queryByName",FakeHeaders(),"")
      route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).map(status(_)) mustBe Some(OK)
    }

    "send 400 on a /slick/queryByNameRest request" in {
      val data = Map("fake" -> "Mauritius")
      val fakeRequest =  FakeRequest(POST, "/queryByNameRest",FakeHeaders(),"")
      route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).map(status(_)) mustBe Some(BAD_REQUEST)
    }

     "send 200 on a /slick/queryByNameRest request" in {
      val data = Map("name" -> "Mauritius")
      val fakeRequest =  FakeRequest(POST, "/queryByNameRest",FakeHeaders(),"")
      route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).map(status(_)) mustBe Some(OK)
     }  
 
  }

  "SlickController" should {

    "render the index page" in {
      val index = route(app, FakeRequest(GET, "/slick/")).get

      status(index) mustBe OK
      contentType(index) mustBe Some("text/html")
      contentAsString(index) must include ("""<a href="/slick/reporting" id="reportingLink">click here for reporting</a>""")
      contentAsString(index) must include ("""<a href="#" id="queryNameLink">click here for querying</a>""")
      contentAsString(index) must include ("""<a href="#" id="queryCodeLink">click here for querying</a>""")

      <a href="#" id="queryNameLink">click here for querying</a>
    }


  "render the queryResult page with unfound country code" in {
      val data = Map("code" -> "MUxxx")
      val fakeRequest =  FakeRequest(POST, "/slick/queryByCode",FakeHeaders(),"")
      val queryResult = route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).get

      status(queryResult) mustBe OK
      contentType(queryResult) mustBe Some("text/html")
    contentAsString(queryResult) must include ("""Country not found""")
  }

  "render the queryResult page" in {
      val data = Map("code" -> "MU")
      val fakeRequest =  FakeRequest(POST, "/slick/queryByCode",FakeHeaders(),"")
      val queryResult = route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).get

      status(queryResult) mustBe OK
      contentType(queryResult) mustBe Some("text/html")
      contentAsString(queryResult) must include ("""    
    Airport(s) for Mauritius
<div class="container" >
    <table class="table table-hover">
	<thead>
	    <tr>
		<th >Airport Code</th>
		<th >Airport Name</th>
		<th > Runway</th>
	    </tr>
	</thead>
	
	<tr>
	    <td>FIMP</td>
	    <td>Sir Seewoosagur Ramgoolam International Airport</td>
	    <td>
		<table class="table-bordered">
		    <tr>
			<td >ASP</td>
			<td >14</td>
		    </tr>
		</table>
		
	  </td>
	</tr>	
	
	<tr>
	    <td>FIMR</td>
	    <td>Sir Charles Gaetan Duval Airport</td>
	    <td>
		<table class="table-bordered">
		    <tr>
			<td >ASP</td>
			<td >12</td>
		    </tr>
		</table>
		
	  </td>
	</tr>	
	
	<tr>
	    <td>MU-0001</td>
	    <td>Agalega Island Airstrip</td>
	    <td>
	  </td>
	</tr>	
	
    </table>
</div>""")

  }

 "render the queryResultRest page with unfound country code" in {
      val data = Map("code" -> "MUxxx")
      val fakeRequest =  FakeRequest(POST, "/slick/queryByCodeRest",FakeHeaders(),"")
      val queryResult = route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).get

      status(queryResult) mustBe OK
      contentType(queryResult) mustBe Some("text/plain")
    contentAsString(queryResult) must include ("""[]""")
  }

  "render the queryResultRest page with a right country code" in {
      val data = Map("code" -> "MU")
      val fakeRequest =  FakeRequest(POST, "/slick/queryByCodeRest",FakeHeaders(),"")
      val queryResult = route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).get

      status(queryResult) mustBe OK
      contentType(queryResult) mustBe Some("text/plain")
      contentAsString(queryResult) must include ("""{"country":{"code":"MU","name":"Mauritius","continent":"AF","wikipediaLink":"http://en.wikipedia.org/wiki/Mauritius","keywords":""},"AirportNrunways":[{"airport":{"identifier":"FIMP","typeOf":"medium_airport","name":"Sir Seewoosagur Ramgoolam International Airport","latitudeDeg":-20.430200576782227,"longitudeDeg":57.68360137939453,"elevationFt":186,"continent":"AF","isoCountry":"MU","isoRegion":"MU-GP","municipality":"Port Louis","scheduledService":"yes","gpsCode":"FIMP","iataCode":"MRU","localCode":null,"homeLink":"http://aml.mru.aero/","wikipediaLink":"http://en.wikipedia.org/wiki/Sir_Seewoosagur_Ramgoolam_International_Airport","keywords":"Plaisance International Airport"},"runways":[{"airportIdentifier":"FIMP","lengthft":11056,"widthFt":148,"surface":"ASP","lighted":1,"closed":0,"leIdentifier":"14","leLatitudeDeg":-20.4233,"leLongitudeDeg":57.6693,"leElevationFt":183,"leHeadingDegT":117.1,"leDisplacedThresholdFt":1082,"heIdentifier":"32","heLatitudeDeg":-20.4372,"heLongitudeDeg":57.698,"heElevationFt":98,"heHeadingDegT":297.1,"heDisplacedThresholdFt":null}]},{"airport":{"identifier":"FIMR","typeOf":"medium_airport","name":"Sir Charles Gaetan Duval Airport","latitudeDeg":-19.757699966430664,"longitudeDeg":63.361000061035156,"elevationFt":95,"continent":"AF","isoCountry":"MU","isoRegion":"MU-RO","municipality":"Port Mathurin","scheduledService":"yes","gpsCode":"FIMR","iataCode":"RRG","localCode":null,"homeLink":null,"wikipediaLink":"http://en.wikipedia.org/wiki/Rodrigues_Island_Airport","keywords":"Plaine Corail"},"runways":[{"airportIdentifier":"FIMR","lengthft":4223,"widthFt":98,"surface":"ASP","lighted":1,"closed":0,"leIdentifier":"12","leLatitudeDeg":-19.7561,"leLongitudeDeg":63.3551,"leElevationFt":68,"leHeadingDegT":106,"leDisplacedThresholdFt":440,"heIdentifier":"30","heLatitudeDeg":-19.7592,"heLongitudeDeg":63.3669,"heElevationFt":81,"heHeadingDegT":286,"heDisplacedThresholdFt":240}]},{"airport":{"identifier":"MU-0001","typeOf":"small_airport","name":"Agalega Island Airstrip","latitudeDeg":-10.3731002808,"longitudeDeg":56.6097984314,"elevationFt":4,"continent":"AF","isoCountry":"MU","isoRegion":"MU-AG","municipality":"Vingt Cinq","scheduledService":"no","gpsCode":"FIMA","iataCode":null,"localCode":null,"homeLink":null,"wikipediaLink":null,"keywords":"North Island"},"runways":[]}]}""")
  }

    """query by name  page for name "Mauriti" """ in {
      val data = Map("name" -> "Mauriti")
      val fakeRequest =  FakeRequest(POST, "/slick/queryByName",FakeHeaders(),"")
      val queryResult = route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).get

      status(queryResult) mustBe OK
      contentType(queryResult) mustBe Some("text/html")
      contentAsString(queryResult) must include ("""    
    Airport(s) for Mauritius
<div class="container" >
    <table class="table table-hover">
	<thead>
	    <tr>
		<th >Airport Code</th>
		<th >Airport Name</th>
		<th > Runway</th>
	    </tr>
	</thead>
	
	<tr>
	    <td>FIMP</td>
	    <td>Sir Seewoosagur Ramgoolam International Airport</td>
	    <td>
		<table class="table-bordered">
		    <tr>
			<td >ASP</td>
			<td >14</td>
		    </tr>
		</table>
		
	  </td>
	</tr>	
	
	<tr>
	    <td>FIMR</td>
	    <td>Sir Charles Gaetan Duval Airport</td>
	    <td>
		<table class="table-bordered">
		    <tr>
			<td >ASP</td>
			<td >12</td>
		    </tr>
		</table>
		
	  </td>
	</tr>	
	
	<tr>
	    <td>MU-0001</td>
	    <td>Agalega Island Airstrip</td>
	    <td>
	  </td>
	</tr>	
	
    </table>
</div>""")
    }

    "render the queryByNameRest page with unfound country name" in {
      val data = Map("name" -> "Muxxxxxxxxxxx")
      val fakeRequest =  FakeRequest(POST, "/slick/queryByNameRest",FakeHeaders(),"")
      val queryResult = route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).get

      status(queryResult) mustBe OK
      contentType(queryResult) mustBe Some("text/plain")
    contentAsString(queryResult) must include ("""[]""")
    }

    """render the queryByNameRest page with a right country name "Mauriti" """ in {
      val data = Map("name" -> "Mauriti")
      val fakeRequest =  FakeRequest(POST, "/slick/queryByNameRest",FakeHeaders(),"")
      val queryResult = route(app,fakeRequest.withFormUrlEncodedBody(data.toList:_*)).get

      status(queryResult) mustBe OK
      contentType(queryResult) mustBe Some("text/plain")
      contentAsString(queryResult) must include ("""[{"country":{"code":"MU","name":"Mauritius","continent":"AF","wikipediaLink":"http://en.wikipedia.org/wiki/Mauritius","keywords":""},"AirportNrunways":[{"airport":{"identifier":"FIMP","typeOf":"medium_airport","name":"Sir Seewoosagur Ramgoolam International Airport","latitudeDeg":-20.430200576782227,"longitudeDeg":57.68360137939453,"elevationFt":186,"continent":"AF","isoCountry":"MU","isoRegion":"MU-GP","municipality":"Port Louis","scheduledService":"yes","gpsCode":"FIMP","iataCode":"MRU","localCode":null,"homeLink":"http://aml.mru.aero/","wikipediaLink":"http://en.wikipedia.org/wiki/Sir_Seewoosagur_Ramgoolam_International_Airport","keywords":"Plaisance International Airport"},"runways":[{"airportIdentifier":"FIMP","lengthft":11056,"widthFt":148,"surface":"ASP","lighted":1,"closed":0,"leIdentifier":"14","leLatitudeDeg":-20.4233,"leLongitudeDeg":57.6693,"leElevationFt":183,"leHeadingDegT":117.1,"leDisplacedThresholdFt":1082,"heIdentifier":"32","heLatitudeDeg":-20.4372,"heLongitudeDeg":57.698,"heElevationFt":98,"heHeadingDegT":297.1,"heDisplacedThresholdFt":null}]},{"airport":{"identifier":"FIMR","typeOf":"medium_airport","name":"Sir Charles Gaetan Duval Airport","latitudeDeg":-19.757699966430664,"longitudeDeg":63.361000061035156,"elevationFt":95,"continent":"AF","isoCountry":"MU","isoRegion":"MU-RO","municipality":"Port Mathurin","scheduledService":"yes","gpsCode":"FIMR","iataCode":"RRG","localCode":null,"homeLink":null,"wikipediaLink":"http://en.wikipedia.org/wiki/Rodrigues_Island_Airport","keywords":"Plaine Corail"},"runways":[{"airportIdentifier":"FIMR","lengthft":4223,"widthFt":98,"surface":"ASP","lighted":1,"closed":0,"leIdentifier":"12","leLatitudeDeg":-19.7561,"leLongitudeDeg":63.3551,"leElevationFt":68,"leHeadingDegT":106,"leDisplacedThresholdFt":440,"heIdentifier":"30","heLatitudeDeg":-19.7592,"heLongitudeDeg":63.3669,"heElevationFt":81,"heHeadingDegT":286,"heDisplacedThresholdFt":240}]},{"airport":{"identifier":"MU-0001","typeOf":"small_airport","name":"Agalega Island Airstrip","latitudeDeg":-10.3731002808,"longitudeDeg":56.6097984314,"elevationFt":4,"continent":"AF","isoCountry":"MU","isoRegion":"MU-AG","municipality":"Vingt Cinq","scheduledService":"no","gpsCode":"FIMA","iataCode":null,"localCode":null,"homeLink":null,"wikipediaLink":null,"keywords":"North Island"},"runways":[]}]}]""")
  }

  }
}
