import org.scalatest.{FlatSpec, Matchers}
import models.{LoadCsvData, ReportingCsv, QueryCsvOption}

class ReportingCsvSpec extends FlatSpec with Matchers {
  val csvModel = new LoadCsvData()
  val queryCvsOption = new QueryCsvOption(csvModel)
  val reportingCsv = new ReportingCsv(queryCvsOption)

  import reportingCsv._

  "10 countries codes containing most airports " should """ be "US", "BR", "CA", "AU", "RU", "FR", "AR", "DE", "CO", "VE" """ in {
    top10CountriesWithMostAirports().map{case (country,airportCount) => country.code} should contain theSameElementsAs
      List("US", "BR", "CA", "AU", "RU", "FR", "AR", "DE", "CO", "VE")
  }

  "10 countries codes containing least airports " should """ be "GS", "PN", "TK","AD","AI","AW","BL","CC","CW","CX"""" in {
    top10countriesWithLeastAirports().map{case (country,airportCount) => country.code} should contain theSameElementsAs
      List("AD", "AI", "AW", "BL", "CC", "CW", "CX", "GI", "GM", "IO")
  }

  //the commented test uses old code,may b subjectto removal
/*
  "mauritius" should " have 2 runway surface" in {
    typeofRunwaysPerCountryByCountryName("Mauritius").head._2 should have length 2
  }

  "mauritius runways " should """ have names  ("ASP", "ASP") """ in {
    /*
       237613	2878	FIMP	11056	148		1	0	14	-20.4233	57.6693	183	117.1	1082	32	-20.4372	57.698	98	297.1	
      237614	        2879	FIMR	4223	98	ASP	1	0	12	-19.7561	63.3551	68	106	440	30	-19.7592	63.3669	81	286	240
      42756 - nil
     */
    typeofRunwaysPerCountryByCountryName("Mauritius").head._2 should contain theSameElementsAs List("ASP", "ASP")
  }

  "mauritius" should " have 1 UNIQUE  runway surface" in {
    uniqueTypeofRunwaysPerCountryByCountryName("Mauritius").head._2 should have length 1
  }

  "mauritius runways " should """ have UNIQUE  names  ("ASP") """ in {
    uniqueTypeofRunwaysPerCountryByCountryName("Mauritius").head._2 should contain theSameElementsAs List("ASP")
  }*/

  "MU" should " have 1 UNIQUE  runway surface" in {
    findUniqueRunWayTypesByCountryCode("MU")._2 should have length 1
  }

  "MU runways " should """ have UNIQUE  names  ("ASP") """ in {
    findUniqueRunWayTypesByCountryCode("MU")._2 should contain theSameElementsAs List("ASP")
  }

  "top 10 most common runway identifications" should """be "H1", "18", "09", "17", "16", "12", "14", "08", "13", "15" """ in {
    top10MostCommonRunwayIdentifications() should contain theSameElementsAs List("H1", "18", "09", "17", "16", "12", "14", "08", "13", "15")
  }

}
