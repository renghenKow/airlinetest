name := """AirlineTest"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.github.melrief" %% "purecsv" % "0.0.9",
  "org.typelevel"%%"cats"%"0.9.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  //web jars
  "org.webjars" % "jquery" % "1.11.1",
  "org.webjars" % "bootstrap" % "3.3.5",
  "org.webjars" % "Bootstrap-3-Typeahead" % "3.1.1",
  //db libs
   "mysql" % "mysql-connector-java" % "6.0.6",
  "com.typesafe.play" %% "play-slick" % "2.0.2" 
)

ensimeIgnoreMissingDirectories := true

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  "Artima Maven Repository" at "http://repo.artima.com/releases"
)

mappings in Universal ++= (baseDirectory.value / "resources" * "*" get) map
  (x => x -> ("resources/" + x.getName))
  
  
// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator


