package models

import scala.collection.parallel.immutable.{ ParMap, ParSeq }
import scala.util.Try

//java library
import javax.inject._

trait CsvModels

case class Country(id: String, code: String, name: String, continent: String, wikipediaLink: String, keywords: String) extends CsvModels

case class Airport(id: Long, identifier: String, typeOf: String, name: String, latitudeDeg: Double, longitudeDeg: Double,
  elevationFt: Option[Int], continent: String, isoCountry: String, isoRegion: String, municipality: String, scheduledService: String,
  gpsCode: String, iataCode: Option[String], localCode: Option[String], homeLink: Option[String], wikipediaLink: Option[String],
  keywords: Option[String]) extends CsvModels

case class Runway(id: Long, airportRef: Long, airportIdentifier: String, lengthft: Option[Int], widthFt: Option[Int], surface: String, lighted: Byte,
  closed: Byte, leIdentifier: String, leLatitudeDeg: Option[Double], leLongitudeDeg: Option[Double], leElevationFt: Option[Int], leHeadingDegT: Option[Double],
  leDisplacedThresholdFt: Option[Double], heIdentifier: String, heLatitudeDeg: Option[Double], heLongitudeDeg: Option[Double],
  heElevationFt: Option[Int], heHeadingDegT: Option[Double], heDisplacedThresholdFt: Option[Int]) extends CsvModels

@Singleton
class LoadCsvData {
  import java.io.File
  import purecsv.safe._

  def loadCountriesFromFile(fileName :String): List[Try[Country]]   = {
    CSVReader[Country].readCSVFromFile(new File(fileName))
       .drop(1) // drop the header part     
  }

   def loadAirportsFromFile(fileName :String): List[Try[Airport]] = {
    CSVReader[Airport].readCSVFromFile(new File(fileName))
      .drop(1) // drop the header part  
  }

  def loadRunwaysFromFile(fileName :String): List[Try[Runway]] = {
    CSVReader[Runway].readCSVFromFile(new File(fileName))
      .drop(1) // drop the header part
  }

   def filterLoadedModel[A <: CsvModels](list : List[Try[A]]) = {
     list.filter { record => record.isSuccess } //remove failed parsing
      .map { record => record.get } // get the record themselves    
  }

  val loadedCountries = filterLoadedModel(loadCountriesFromFile("resources/countries.csv") ).par
  val loadedAirports  = filterLoadedModel(loadAirportsFromFile("resources/airports.csv")).par
  val loadedAirportMap: ParMap[String, ParSeq[Long]] = loadedAirports
    .groupBy { airport => airport.isoCountry }
    .mapValues(airportSeq => airportSeq.map(airport => airport.id))
    .toMap
  val loadedRunways  = filterLoadedModel(loadRunwaysFromFile("resources/runways.csv")).par
  val loadedRunwaysMap: ParMap[Long, ParSeq[Runway]]  = loadedRunways.groupBy { runway => runway.airportRef }.toMap

 /* def loadFromCsv[T](fileName :String) : List[T] = {
    CSVReader[T].readCSVFromFile(new File(fileName))
      .filter{record => record.isSuccess} //remove failed parsing
      .map{record => record.get} // get the record themselves      
  }*/

}

