package models.slick

import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import slick.driver.JdbcProfile
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models._

//cats lib imported to convert tuple of futures into a future of tuple
import cats._
import cats.instances.future._
import cats.syntax.bitraverse._

class QuerySlick @Inject() (override val dbConfigProvider: DatabaseConfigProvider) extends DAOs(dbConfigProvider) with QueryForSlick {
  import dbConfig.driver.api._
  import cats.implicits._ 

  private def _findCountryByName(name: String): Query[Countries, Country, Seq] = {
    countries.filter { country => country.name.toLowerCase like s"%${name.toLowerCase()}%" }
  }

  /**
   * @param name String
   * @return list of countries
   * find countries containing name,search is case insensitive
   */
  def findCountryByName(name: String): Future[Seq[Country]] = {
    val query = countries.filter { country => country.name.toLowerCase like s"%${name.toLowerCase()}%" }
    db.run(_findCountryByName(name).result)
  }

  /**
   * @param name String
   * @return list of countries
   * find countries starting with name,search is case insensitive
   */
  def findCountryStartingWithName(name: String): Future[Seq[Country]] = {
    val query = countries.filter { country => country.name.toLowerCase like s"${name.toLowerCase}%" }
    db.run(query.result)
  }

  private def _findCountryByExactCode(code: String) = {
    countries.filter { country => country.code.toLowerCase === code.toLowerCase }
  }

  def findCountryByExactCode(code: String): Future[Seq[Country]] = {
    val query = _findCountryByExactCode(code)
    db.run(query.result)
  }

  private def _findCountryBySomeCode(code: String) = {
    countries.filter { country => country.code.toLowerCase like s"%${code.toLowerCase}%" }
  }

  def findCountryBySomeCode(code: String) = {
    val query = _findCountryBySomeCode(code)
    db.run(query.result)
  }

  def _findAirportByCountry(country: Country) = {
    airports.filter { airport => airport.isoCountry === country.code }
  }

  def findAirportByCountry(country: Country) = {
    val query = _findAirportByCountry(country)
    db.run(query.result)
  }

  private def _findAirportsByCountryCode(countryCode: String) = {
    airports.filter { airport => airport.isoCountry === countryCode }
  }

  def findAirportsByCountryCode(countryCode: String) = {
    val query = _findAirportsByCountryCode(countryCode)
    db.run(query.result)
  }

  /**
   * @param name String
   * @return list of airport
   * find countriesairport with country containing name,search is case insensitive
   */
  def airportsByCountryName(name: String) = {
    val query = for {
      country <- _findCountryByName(name)
      airport <- airports if airport.isoCountry === country.code
    } yield airport
    db.run(query.result)
  }

  def findRunWaysByAirportIdentifier(airportIdentifier: String): Future[Seq[Runway]] = {
    val query = runways.filter { runways => runways.airportIdentifier === airportIdentifier }
    db.run(query.result)
  }

  def findRunWaysByAirportRef(airportRef: Long): Future[Seq[Runway]] = {
    val query = runways.filter { runway => runway.airportRef === airportRef }
    db.run(query.result)
  }

  def findRunWaysByAirports(airports: List[Airport]): Future[Seq[Runway]] = {
    val airportCodes = airports.map { airport => airport.id }
    val query = runways.filter { runway => runway.airportRef inSetBind airportCodes }
    db.run(query.result)
  }
  /*
  def findAirportsAndRunwaysByCountryName(name: String):  Future[List[(Country, List[(Airport, List[Runway])])]] = {
    val query = for {
      country <- countries if country.name.toLowerCase === name.toLowerCase()
      airport <- airports if airport.isoCountry === country.code
      runway <- runways if runway.airportRef === airport.id
    } yield (country, airport, runway)


    val resultFuture = db.run(query.result)
    resultFuture.map{result =>
      val grouping: Map[Country, List[(Airport, List[Runway])]] = result
        .groupBy { case (country, airport, runway) => country}
        .mapValues{values => values.map{case (c,a,r) => (a,r) }}
        .mapValues {airportNrunways: Seq[(Airport, Runway)] =>
          airportNrunways.groupBy  {case (airport, runway) => airport }.mapValues{values => values.map{case (a,r) => r}.toList}.toList          
        }      
      grouping.toList
    }
  }
 */
 
  def findAirportsAndRunwaysByCountryCode(code: String): Future[List[(Country, List[(Airport, List[Runway])])]] = {
    val query = for {
      country <- _findCountryByExactCode(code)
      airport <- airports if airport.isoCountry === country.code
      //runway <- runways if runway.airportRef === airport.id
    } yield (country, airport)

    val resultFuture = db.run(query.result)
    findRunwaysByCountrynAirport(resultFuture)
  }

  private def findRunwaysByCountrynAirport(resultFuture: Future[Seq[(models.Country, models.Airport)]]):
      Future[List[(models.Country, List[(models.Airport, List[models.Runway])])]] = {
    val groupedFuture = resultFuture.map { result =>
      result
        .groupBy { case (country, airport) => country }
        .mapValues { values => values.map { case (c, a) => a } }.toList
    }

    groupedFuture.flatMap { grouped =>
     val countriesNairportsNrunways = grouped.map {
       case (c, as) =>
         val asNrunwaysFuture = as.map { airport =>
           //val runways = Await.result(findRunWaysByAirportRef(airport.id).map { _.toList }, Duration.Inf)
           val runways = findRunWaysByAirportRef(airport.id).map(_.toList)
           (Future.successful(airport), runways).bisequence
         }
        
         (Future.successful(c),
           Future.sequence(asNrunwaysFuture).map(_.toList)
         ).bisequence
     }
     Future.sequence(countriesNairportsNrunways)
    }
  }

  def findAirportsAndRunwaysByCountryName(name: String): Future[List[(Country, List[(Airport, List[Runway])])]] = {
    val query = for {
      country <- _findCountryByName(name)
      airport <- airports if airport.isoCountry === country.code
      //runway <- runways if runway.airportRef === airport.id
    } yield (country, airport)    

    val resultFuture = db.run(query.result)
     findRunwaysByCountrynAirport(resultFuture)    
  }

  def findRunWaysbyCountryCode(countryCode: String): Future[Seq[String]] = {
    val query = for {
      country <- _findCountryByExactCode(countryCode)
      airport <- _findAirportsByCountryCode(countryCode)
      runway <- runways if runway.airportRef === airport.id
    } yield (runway.surface)

    val queryDistinct = query.distinct
    db.run(queryDistinct.result)
  }

  /**
   * @param list of country codes
   * return a list of countries base on country codes
   */
  def countryCodesToCountries(countryCodes: List[String]): Future[Seq[Country]] = {
    val query = countries.filter { country => country.code inSet countryCodes }
    db.run(query.result)
  }

  def listAllCountriesNameAndCodeOnly(): Future[List[CountryCodeNname]] = {
    val query = countries.map { country => (country.code, country.name) }
    db.run(query.result).map { result =>
      result.map { case (code, name) => CountryCodeNname(code, name) }.toList
    }
  }

}
