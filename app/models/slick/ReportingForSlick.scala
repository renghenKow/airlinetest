package models

import scala.concurrent.Future

trait ReportingForSlick {
  def top10CountriesWithMostAirports(): Future[Seq[(Country,Int)]]
  def top10countriesWithLeastAirports(): Future[Seq[(Country,Int)]]
  def findUniqueRunWayTypesForallCountries() : Future[Seq[(Country, Seq[String])] ]
  def top10MostCommonRunwayIdentifications() : Future[Seq[String]]
}
