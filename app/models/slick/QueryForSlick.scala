package models.slick

import scala.concurrent.Future
import models.{Country,Airport,Runway,CountryCodeNname}

trait QueryForSlick {
  def findCountryByName(name: String): Future[Seq[Country]] 
  def findCountryStartingWithName(name: String):  Future[Seq[Country]]
  def findCountryByExactCode(code: String):  Future[Seq[Country]]
  def findCountryBySomeCode(code: String): Future[Seq[Country]]
  def findAirportByCountry(country: Country): Future[Seq[Airport]]
  def airportsByCountryName(name: String): Future[Seq[Airport]]
  def findRunWaysByAirportIdentifier(airport: String): Future[Seq[Runway]] 
  def findRunWaysByAirportRef(airportRef: Long): Future[Seq[Runway]] 
  def findRunWaysByAirports(airports: List[Airport]) : Future[Seq[Runway]]
  def findAirportsAndRunwaysByCountryName(name: String): Future [ List[(Country,List[(Airport, List[Runway])])]]
  def findAirportsAndRunwaysByCountryCode(code: String):  Future[List[(Country, List[(Airport, List[Runway])])]] 
  def countryCodesToCountries(countryCodes: List[String]):Future[Seq[Country]] 
  //def queryEverythingByCountryName(name: String): List[(Country, Airport, Runways)]
  def listAllCountriesNameAndCodeOnly() : Future[List[CountryCodeNname]]
}
