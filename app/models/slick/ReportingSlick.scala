package models.slick

import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models._

class ReportingSlick @Inject() (override val dbConfigProvider: DatabaseConfigProvider) extends DAOs(dbConfigProvider) with ReportingForSlick {
  import dbConfig.driver.api._

  def airportsPerCountry(): Future[Map[Country, Int]] = {

    val query = (for {
      airport <- airports
      country <- countries if country.code === airport.isoCountry
    } yield (country, airport)).groupBy { _._1 }

    val query2 = query.map { case (country, count) => (country, count.length) }

    db.run(query2.result).map { results => results.toMap }
  }

  def top10CountriesWithMostAirports(): Future[Seq[(Country, Int)]] = {
    val apcFuture = airportsPerCountry()
    apcFuture.map { apc => (apc.toList.sortBy(_._2)(Ordering[Int].reverse)).take(10) }
  }

  def top10countriesWithLeastAirports(): Future[Seq[(Country, Int)]] = {
    val apcFuture = airportsPerCountry()
    apcFuture.map { apc => apc.toList.sortBy(cc => (cc._2,cc._1.code)).take(10) }
  }

  def findUniqueRunWayTypesForallCountries(): Future[Seq[(Country, Seq[String])]] = {
    val query = for {
      country <- countries
      airport <- airports if airport.isoCountry === country.code
      runway <- runways if runway.airportRef === airport.id
    } yield (country, runway.leIdentifier)

    db.run(query.result).map { result: Seq[(Country, String)] =>
      (result
        .groupBy(_._1)
        .mapValues { values: Seq[(Country, String)] => values.map(_._2) }).toSeq
    }
  }

  def top10MostCommonRunwayIdentifications(): Future[Seq[String]] = {
    val query = runways.groupBy { runway => runway.leIdentifier }
      .map { case (id, runway) => (id, runway.length) }

    db.run(query.result).map { result =>
      (result.sortBy(_._2)(Ordering[Int].reverse)).take(10).map { _._1 }
    }
  }

}
