package models.slick

import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.Future
import models._

class DAOs @Inject() (protected  val dbConfigProvider: DatabaseConfigProvider) { //extends HasDatabaseConfigProvider[JdbcProfile] {

    val dbConfig = dbConfigProvider.get[JdbcProfile]
  val db = dbConfig.db
  import dbConfig.driver.api._
  //import driver.api._

  class Countries(tag: Tag) extends Table[Country](tag, "Country") {
    def id = column[String]("id", O.PrimaryKey)
    def code = column[String]("code")
    def name = column[String]("name")
    def continent = column[String]("continent")
    def wikipediaLink = column[String]("wikipediaLink")
    def keywords = column[String]("keywords")

    def * = (id, code, name, continent, wikipediaLink, keywords) <> (Country.tupled, Country.unapply)
  }

  val countries = TableQuery[Countries]

  val query = countries.length
  query.result

  class Airports(tag: Tag) extends Table[Airport](tag, "Airport") {
    def id = column[Long]("id", O.PrimaryKey)
    def identifier = column[String]("identifier")
    def typeOf = column[String]("typeOf")
    def name = column[String]("name")
    def latitudeDeg = column[Double]("latitudeDeg")
    def longitudeDeg = column[Double]("longitudeDeg")
    def elevationFt = column[Option[Int]]("elevationFt")
    def continent = column[String]("continent")
    def isoCountry = column[String]("isoCountry")
    def isoRegion = column[String]("isoRegion")
    def municipality = column[String]("municipality")
    def scheduledService = column[String]("scheduledService")
    def gpsCode = column[String]("gpsCode")
    def iataCode = column[Option[String]]("iataCode")
    def localCode = column[Option[String]]("localCode")
    def homeLink = column[Option[String]]("homeLink")
    def wikipediaLink = column[Option[String]]("wikipediaLink")
    def keywords = column[Option[String]]("keywords")
    def * = (id, identifier, typeOf, name, latitudeDeg, longitudeDeg, elevationFt, continent,
      isoCountry, isoRegion, municipality, scheduledService, gpsCode, iataCode, localCode,
      homeLink, wikipediaLink, keywords) <> (Airport.tupled, Airport.unapply)
  }

   val airports = TableQuery[Airports]

   class Runways(tag: Tag) extends Table[Runway](tag, "Runway") {
    def id = column[Long]("id", O.PrimaryKey)
    def airportRef = column[Long]("airportRef")
    def airportIdentifier = column[String]("airportIdentifier")
    def lengthft = column[Option[Int]]("lengthft")
    def widthFt = column[Option[Int]]("widthFt")
    def surface = column[String]("surface")
    def lighted = column[Byte]("lighted")

    def closed = column[Byte]("closed")
    def leIdentifier = column[String]("leIdentifier")
    def leLatitudeDeg = column[Option[Double]]("leLatitudeDeg")
    def leLongitudeDeg = column[Option[Double]]("leLongitudeDeg")
    def leElevationFt = column[Option[Int]]("leElevationFt")
    def leHeadingDegT = column[Option[Double]]("leHeadingDegT")

    def leDisplacedThresholdFt = column[Option[Double]]("leDisplacedThresholdFt")
    def heIdentifier = column[String]("heIdentifier")
    def heLatitudeDeg = column[Option[Double]]("heLatitudeDeg")
    def heLongitudeDeg = column[Option[Double]]("heLongitudeDeg")

    def heElevationFt = column[Option[Int]]("heElevationFt")
    def heHeadingDegT = column[Option[Double]]("heHeadingDegT")
    def heDisplacedThresholdFt = column[Option[Int]]("heDisplacedThresholdFt")

    def * = (id, airportRef, airportIdentifier, lengthft, widthFt, surface, lighted, closed,
      leIdentifier, leLatitudeDeg, leLongitudeDeg, leElevationFt, leHeadingDegT, leDisplacedThresholdFt, heIdentifier,
      heLatitudeDeg, heLongitudeDeg, heElevationFt,heHeadingDegT,heDisplacedThresholdFt) <> (Runway.tupled, Runway.unapply)
  }

   val runways = TableQuery[Runways]


  /*
   code to populate db,to be used only once
  val loadCsvData = new LoadCsvData
  import loadCsvData._

  import scala.concurrent._
  import scala.concurrent.duration._
  val toBeInserted = loadedAirports.map { row => airports.insertOrUpdate(row) }.toList
  val inOneGo = DBIO.sequence(toBeInserted)
  val dbioFuture = db.run(inOneGo)
  val rowsInserted = Await.result(dbioFuture, Duration.Inf)

  import scala.concurrent._
  import scala.concurrent.duration._
  val toBeInserted = loadedRunways.map { row => runways.insertOrUpdate(row) }.toList
  val inOneGo = DBIO.sequence(toBeInserted)
  val dbioFuture = db.run(inOneGo)
  val rowsInserted = Await.result(dbioFuture, Duration.Inf)
*/
}
