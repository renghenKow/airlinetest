package models

//java library
import javax.inject._

@Singleton
class ReportingCsv(val queryCsvOption: QueryCsvOption) extends Reporting {
  val csvModel = queryCsvOption.csvModel
  import csvModel._

  def airportsPerCountry(): Map[String, Int] = {
    val countriesWithairports = loadedAirports
      .groupBy { airtport => airtport.isoCountry }
      .mapValues(_.length).toList.toMap

    val countries = countriesWithairports.keys.toList
    val zeroAirportCountries  = loadedCountries
      .filter { country => !countries.exists(_ == country.code) }
      .map {country => (country.code,0)}.toList
    countriesWithairports ++ zeroAirportCountries
  }

  def top10CountriesWithMostAirports(): List[(Country, Int)] = {
    val apc = airportsPerCountry()
    val countryCodes = (apc.toList.sortBy(_._2)(Ordering[Int].reverse)).take(20)
    countryCodesToCountry(countryCodes).toList
    //queryCsvOption.countryCodestoCountries(countryCodes.map(_._1))
  }

  private def countryCodesToCountry(countryCodes: List[(String, Int)]): List[(models.Country, Int)] = {
    countryCodes
      .map { case (countryCode, airportCount) => (queryCsvOption.findCountryByExactCode(countryCode), airportCount) }
      .filter { case (countryOption, airportCount) => countryOption.isDefined }
      .map { case (country, airportCount) => (country.get, airportCount) }
      .take(10)
  }

  def top10countriesWithLeastAirports(): List[(Country, Int)] = {
    val apc = airportsPerCountry()
    val countryCodes = apc.toList.sortBy(cc => (cc._2, cc._1)).take(20)
    countryCodesToCountry(countryCodes)
    //queryCsvOption.countryCodestoCountries(countryCodes.toList)
  }

  def typeofRunwaysPerCountryByCountryName(countryName: String): List[(Country, List[String])] = {
    val countryAirportNRunways = queryCsvOption.findAirportsAndRunwaysByCountryName(countryName).par
    countryAirportNRunways.map {
      case (country, airportNrunways) =>
        val runways = airportNrunways.flatMap { case (airport, runways) => runways.par.map { runways => runways.surface } }
        (country, runways)
    }.toList
  }

  def findUniqueRunWayTypesForallCountries(): List[(Country, List[String])] = {
    loadedCountries.map { country => (country, queryCsvOption.findRunWaysbyCountryCode(country.code))
    }.toList.sortBy { _._1.name }
  }

  def findUniqueRunWayTypesFor20Countries(): List[(Country, List[String])] = {
    loadedCountries.take(20).map { country => (country, queryCsvOption.findRunWaysbyCountryCode(country.code))
    }.toList
  }

  def findUniqueRunWayTypesByCountryCode(countryCode: String): (Country, List[String]) = {
    val runways = queryCsvOption.findRunWaysbyCountryCode(countryCode)
    (queryCsvOption.findCountryByExactCode(countryCode).get, runways)
  }

  def top10MostCommonRunwayIdentifications(): List[String] = {
    (loadedRunways.groupBy { runways => runways.leIdentifier }.mapValues(_.length).toList
      .sortBy(_._2)(Ordering[Int].reverse))
      .take(10)
      .map { record: (String, Int) => record._1 }
  }
}
