package models

import scala.collection.parallel.immutable.ParSeq

trait QueryOption {
  def findCountryByName(name: String): List[Country]
  def findCountryStartingWithName(name: String): List[Country]
  def findCountryByExactCode(code: String): Option[Country]
  def findCountryBySomeCode(code: String): List[Country]
  def findAirportByCountry(country: Country): ParSeq[Airport]
  def airportsByCountryName(name: String): ParSeq[Airport]
  def findRunWaysByAirportIdentifier(airport: String): ParSeq[Runway]
  def findRunWaysByAirportRef(airportRef: Long): ParSeq[Runway]

  def findRunWaysByAirports(airports: ParSeq[Airport]): ParSeq[(Airport, ParSeq[Runway])]
  def findAirportsAndRunwaysByCountryName(name: String): List[(Country,List[(Airport, List[Runway])])]
  def countryCodestoCountries(countryCodes: List[String]): List[models.Country] 
  //def queryEverythingByCountryName(name: String): List[(Country, Airport, Runways)]
  def listAllCountriesNameAndCodeOnly() :List[CountryCodeNname] 
}
