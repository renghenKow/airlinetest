package models

trait Reporting {
  def top10CountriesWithMostAirports(): List[(Country,Int)]
  def top10countriesWithLeastAirports(): List[(Country,Int)]
  def typeofRunwaysPerCountryByCountryName(countryCodeName :String) : List[(Country ,List[String]) ]
  def top10MostCommonRunwayIdentifications() : List[String]
}
