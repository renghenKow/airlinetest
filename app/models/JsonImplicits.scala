package models

import play.api.libs.json._

object JsonImplicits {

  implicit val CountryCodeNnameWrites = new Writes[CountryCodeNname] {
    def writes(country: CountryCodeNname) = Json.obj(
      "code" -> country.code,
      "name" -> country.name
    )
  }

  implicit val CountryWrites = new Writes[Country] {
    def writes(country: Country) = Json.obj(
      "code" -> country.code,
      "name" -> country.name,
      "continent" -> country.continent,
      "wikipediaLink" -> country.wikipediaLink,
      "keywords" -> country.keywords
    )
  }

  implicit val AirportWrites = new Writes[Airport] {
    def writes(airport: Airport) = Json.obj(
      "identifier" -> airport.identifier,
      "typeOf" -> airport.typeOf,
      "name" -> airport.name,
      "latitudeDeg" -> airport.latitudeDeg,
      "longitudeDeg" -> airport.longitudeDeg,
      "elevationFt" -> airport.elevationFt,
      "continent" -> airport.continent,
      "isoCountry" -> airport.isoCountry,
      "isoRegion" -> airport.isoRegion,
      "municipality" -> airport.municipality,
      "scheduledService" -> airport.scheduledService,
      "gpsCode" -> airport.gpsCode,
      "iataCode" -> airport.iataCode,
      "localCode" -> airport.localCode,
      "homeLink" -> airport.homeLink,
      "wikipediaLink" -> airport.wikipediaLink,
      "keywords" -> airport.keywords
    )
  }

  implicit val RunwayWrites = new Writes[Runway] {
    def writes(runway: Runway) = Json.obj(
      "airportIdentifier" -> runway.airportIdentifier,
      "lengthft" -> runway.lengthft,
      "widthFt" -> runway.widthFt,
      "surface" -> runway.surface,
      "lighted" -> runway.lighted,
      "closed" -> runway.closed,
      "leIdentifier" -> runway.leIdentifier,
      "leLatitudeDeg" -> runway.leLatitudeDeg,
      "leLongitudeDeg" -> runway.leLongitudeDeg,
      "leElevationFt" -> runway.leElevationFt,
      "leHeadingDegT" -> runway.leHeadingDegT,
      "leDisplacedThresholdFt" -> runway.leDisplacedThresholdFt,
      "heIdentifier" -> runway.heIdentifier,
      "heLatitudeDeg" -> runway.heLatitudeDeg,
      "heLongitudeDeg" -> runway.heLongitudeDeg,
      "heElevationFt" -> runway.heElevationFt,
      "heHeadingDegT" -> runway.heHeadingDegT,
      "heDisplacedThresholdFt" -> runway.heDisplacedThresholdFt
    )
  }

  implicit val airportsNrunways = new Writes[(models.Airport, List[models.Runway])] {
    def writes(ar: (models.Airport, List[models.Runway])) = Json.obj(
      "airport" -> ar._1,
      "runways" -> ar._2
    )
  }

  implicit val countryAirportsNrunways = new Writes[(models.Country, List[(models.Airport, List[models.Runway])])] {
    def writes(car: (models.Country, List[(models.Airport, List[models.Runway])])) = Json.obj(
      "country" -> car._1,
      "AirportNrunways" -> car._2
    )
  }

}

