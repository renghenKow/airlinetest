package models

//java library
import javax.inject._

@Singleton
class QueryCsvOption(val csvModel: LoadCsvData) extends QueryOption {
  import csvModel._
  import cats.instances.all._, cats.syntax.eq._
  import scala.collection.parallel.immutable.ParSeq

  /**
   * @param name String
   * @return list of countries
   * find countries containing name,search is case insensitive
   */
  def findCountryByName(name: String): List[Country] = {
    val Pattern = s"(?i)(${name}.*)".r
    loadedCountries.filter { country =>
      Pattern.findFirstMatchIn(country.name).isDefined
    }.toList
  }

  /**
   * @param name String
   * @return list of countries
   * find countries starting with name,search is case insensitive
   */
  def findCountryStartingWithName(name: String): List[Country] = {
    val Pattern = s"(?i)^${name}".r
    loadedCountries.filter { country =>
      Pattern.findFirstMatchIn(country.name).isDefined
    }.toList
  }

  def findCountryByExactCode(code: String): Option[Country] = {
    loadedCountries.find { country => country.code.toLowerCase() === code.toLowerCase() }
  }

  def findCountryBySomeCode(code: String): List[Country] = {
    val Pattern = s"(?i)(${code}.*)".r
    loadedCountries.filter { country =>
      Pattern.findFirstMatchIn(country.code).isDefined
    }.toList
  }

  def findAirportByCountry(country: Country): ParSeq[models.Airport] = {
    loadedAirports.par.filter { airport => airport.isoCountry === country.code }
  }

  def findAirportsByCountryCode(countryCode: String): ParSeq[models.Airport] = {
    loadedAirports.par.filter { airport => airport.isoCountry === countryCode }
  }

  def findAirportCodesByCountryCode(countryCode : String): ParSeq[Long] = {
    loadedAirportMap.getOrElse(countryCode, ParSeq.empty)
  }

  /**
   * @param name String
   * @return list of airport
   * find countriesairport with country containing name,search is case insensitive
   */
  def airportsByCountryName(name: String): ParSeq[Airport] = {
    for {
      countries <- findCountryByName(name).par
      airports <- findAirportByCountry(countries)
    } yield airports
  }

    
  def findRunWaysByAirportIdentifier(airportIdentifier: String) : ParSeq[Runway]= {
    loadedRunways.filter { runways => runways.airportIdentifier === airportIdentifier }    
  }

  def findRunWaysByAirportRef(airportRef: Long): ParSeq[Runway] = {
     loadedRunwaysMap.getOrElse(airportRef, ParSeq.empty)
  }

  def findRunWaysByAirports(airports: ParSeq[Airport]) = {
    airports.map { airport => (airport, findRunWaysByAirportIdentifier(airport.identifier)) }
  }

  def findRunWaysByAirportCodes(airportCodes: ParSeq[Long]): ParSeq[Runway] = {
    airportCodes.flatMap { airportCode => findRunWaysByAirportRef(airportCode) }
  }

  def findAirportsAndRunwaysByCountryName(name: String): List[(Country, List[(Airport, List[Runway])])] = {
    findCountryByName(name).par.map { country =>
      val result= findRunWaysByAirports(findAirportByCountry(country))
       (country,result.map{case (a,r) => (a,r.toList)}.toList)
    }.toList
  }

  def findAirportsAndRunwaysByCountryCode(code: String): Option[(Country, List[(Airport, List[Runway])])] = {
    findCountryByExactCode(code).map{country =>
      val airports= findAirportByCountry(country).map{airport => (airport,findRunWaysByAirportRef(airport.id).toList)}
      (country, airports.toList)
    }      
    }

    /*def findCountryByName(name).par.map { country =>
      val result= findRunWaysByAirports(findAirportByCountry(country))
       (country,result.map{case (a,r) => (a,r.toList)}.toList)
    }.toList*/
  

  def findRunWaysbyCountryCode(countryCode : String): List[String] = {
    /*findRunWaysByAirports(findAirportsByCountryCode(countryCode))
     .flatMap{ case (airports,runways) => runways.map { runway => runway.surface }}.distinct.toList*/
    findRunWaysByAirportCodes(findAirportCodesByCountryCode(countryCode)).map { _.surface }.distinct.toList
  }

  
  /**
    * @param list of country codes
    * return a list of countries base on country codes
    * */
   def countryCodestoCountries(countryCodes: List[String]): List[models.Country] = {
    countryCodes
      .map { cc => findCountryByExactCode(cc) }
      .filter { country => country.isDefined }
      .map(_.get)
      .toList
   }

  def listAllCountriesNameAndCodeOnly() :List[CountryCodeNname]  = {
    loadedCountries.map{country => CountryCodeNname(country.code,country.name)}.toList
  }

  /*def queryEverythingByCountryName(name: String): List[(Country, Airport, Runways)] = {
    for {
      countries <- findCountryByName(name)
      airports <- findAirportByCountry(countries)
      runways <- findRunWaysByAirportIdentifier(airports.identifier)
    } yield (countries, airports, runways)
  }*/

}
