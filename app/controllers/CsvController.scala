package controllers

//java library
import javax.inject._

//play api
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.data._
import play.api.data.Forms._

import play.twirl.api.Html
import play.api.Logger

//models
import models._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
  */
@Singleton
class CsvController extends Controller {
  val loadModels = new LoadCsvData()
  val queryCsvOption = new QueryCsvOption(loadModels)
  val reportingCsv = new  ReportingCsv(queryCsvOption)

    import JsonImplicits._
  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index = Action {
    val listOfCountries = reportingCsv.queryCsvOption.listAllCountriesNameAndCodeOnly
    val jsonCountries = Json.toJson(listOfCountries)
    Ok(views.html.index(jsonCountries.toString()))
  }

  def reporting = Action {
    val top10CountriesWithMostAirport = views.html.pagelets.countriesWithNumberOfAirports(reportingCsv.top10CountriesWithMostAirports)
    val top10CountriesWithLeastAirport = views.html.pagelets.countriesWithNumberOfAirports(reportingCsv.top10countriesWithLeastAirports)
    val countriesnTheirRunwayTypes = views.html.pagelets.runwaysAvailableByCountry(reportingCsv.findUniqueRunWayTypesForallCountries)
    val topMostCommonRunWay = views.html.pagelets.topMostCommonRunWay(reportingCsv.top10MostCommonRunwayIdentifications())
    Ok(views.html.reporting(top10CountriesWithMostAirport, top10CountriesWithLeastAirport, countriesnTheirRunwayTypes, topMostCommonRunWay))
  }

  /**
    *validation for query code
    */
  case class QueryByCode(code: String)
  val queryByCodeForm = Form(
    mapping(
      "code" -> nonEmptyText
    )(QueryByCode.apply)(QueryByCode.unapply)
  )

  def queryByCode = Action { implicit request =>
    queryByCodeForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest("")
      },
      userData => {
        val result = reportingCsv.queryCsvOption.findAirportsAndRunwaysByCountryCode(userData.code)
        Ok(views.html.pagelets.queryDisplay(result))
      }
    )
  }

  def queryByCodeRest = Action { implicit request =>
    queryByCodeForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest("")
      },
      userData => {
        val result = reportingCsv.queryCsvOption.findAirportsAndRunwaysByCountryCode(userData.code)
        val json = if (result.isEmpty) JsNull else Json.toJson(result.get)        
        Ok(json.toString)
      }
    )
  }

  /**
    *validation for name field
    */
  case class QueryByName(name: String)
  val queryByNameForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(QueryByName.apply)(QueryByName.unapply)
  )
  def queryByName = Action { implicit request =>
    queryByNameForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest("")
      },
      userData => {
        val result = reportingCsv.queryCsvOption.findAirportsAndRunwaysByCountryName(userData.name)
       Ok(views.html.pagelets.queryByNameDisplay(result))
       
      }
    )
  }


   def queryByNameRest = Action { implicit request =>
    queryByNameForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest("")
      },
      userData => {
        val result = reportingCsv.queryCsvOption.findAirportsAndRunwaysByCountryName(userData.name)
        val json = Json.toJson(result)        
        Ok(json.toString)
      }
    )
  }

}
