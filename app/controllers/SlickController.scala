package controllers

//java library
import javax.inject._

//play api
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.data._
import play.api.data.Forms._

import play.twirl.api.Html
import play.api.Logger

import play.api.libs.concurrent.Execution.Implicits.defaultContext

//models
import models._
import models.slick._

import scala.concurrent.Future

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
  */
@Singleton
class SlickController @Inject() (querySlick: QuerySlick, reportingSlick: ReportingSlick) extends Controller {

  import JsonImplicits._
  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index = Action.async {
    querySlick.listAllCountriesNameAndCodeOnly.map { listOfCountries =>
      val jsonCountries = Json.toJson(listOfCountries)
      Ok(views.html.indexSlick(jsonCountries.toString()))
    }
  }

  def reporting = Action.async {
    val top10CountriesWithMostAirportFuture = reportingSlick.top10CountriesWithMostAirports.map { result =>
      views.html.pagelets.countriesWithNumberOfAirports(result.toList)
    }
    val top10CountriesWithLeastAirportFuture = reportingSlick.top10countriesWithLeastAirports.map { result =>
      views.html.pagelets.countriesWithNumberOfAirports(result.toList)
    }
    val countriesnTheirRunwayTypesFuture = reportingSlick.findUniqueRunWayTypesForallCountries.map { result =>
      val transformedResult = result.map { case (country, runways) => (country, runways.toList) }.toList
      views.html.pagelets.runwaysAvailableByCountry(transformedResult)
    }
    val topMostCommonRunWayFuture = reportingSlick.top10MostCommonRunwayIdentifications.map { result =>
      views.html.pagelets.topMostCommonRunWay(result.toList)
    }
    for {
      top10CountriesWithMostAirport <- top10CountriesWithMostAirportFuture
      top10CountriesWithLeastAirport <- top10CountriesWithLeastAirportFuture
      countriesnTheirRunwayTypes <- countriesnTheirRunwayTypesFuture
      topMostCommonRunWay <- topMostCommonRunWayFuture
    } yield {
      Ok(views.html.reporting(top10CountriesWithMostAirport, top10CountriesWithLeastAirport, countriesnTheirRunwayTypes, topMostCommonRunWay))
    }
  }

  /**
   * validation for query code
   */
  case class QueryByCode(code: String)
  val queryByCodeForm = Form(
    mapping(
      "code" -> nonEmptyText
    )(QueryByCode.apply)(QueryByCode.unapply)
  )

  def queryByCode = Action.async { implicit request =>
    queryByCodeForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        Future.successful(BadRequest(""))
      },
      userData => {
        val resultFuture = querySlick.findAirportsAndRunwaysByCountryCode(userData.code)
        resultFuture.map { result =>
          val transformedResult = result.headOption
          Ok(views.html.pagelets.queryDisplay(transformedResult))
        }
      } )
  }

  def queryByCodeRest = Action.async { implicit request =>
    queryByCodeForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
         Future.successful(BadRequest(""))
      },
      userData => {
        val resultFuture = querySlick.findAirportsAndRunwaysByCountryCode(userData.code)
        resultFuture.map { result =>
          val json = Json.toJson(result)
          Ok(json.toString)
        }
      } )
  }

   /**
    *validation for name field
    */
  case class QueryByName(name: String)
  val queryByNameForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(QueryByName.apply)(QueryByName.unapply)
  )
  def queryByName = Action.async { implicit request =>
    queryByNameForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        Future.successful(BadRequest(""))
      },
      userData => {
        val resultFuture = querySlick.findAirportsAndRunwaysByCountryName(userData.name)
        resultFuture.map { result =>
       Ok(views.html.pagelets.queryByNameDisplay(result))
        }
      }
    )
  }

   def queryByNameRest = Action.async { implicit request =>
    queryByNameForm.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
       Future.successful(BadRequest(""))
      },
      userData => {
        val resultFuture = querySlick.findAirportsAndRunwaysByCountryName(userData.name)
        resultFuture.map { result =>
           val json = Json.toJson(result)        
          Ok(json.toString)
        }
        
      }
    )
  }



}
