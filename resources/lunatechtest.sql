-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 20, 2017 at 08:47 PM
-- Server version: 10.0.29-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lunatechtest`
--
CREATE DATABASE IF NOT EXISTS `lunatechtest` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `lunatechtest`;

-- --------------------------------------------------------

--
-- Table structure for table `Airport`
--

DROP TABLE IF EXISTS `Airport`;
CREATE TABLE `Airport` (
  `id` bigint(20) NOT NULL,
  `identifier` varchar(10) NOT NULL,
  `typeOf` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `latitudeDeg` double NOT NULL,
  `longitudeDeg` double NOT NULL,
  `elevationFt` int(11) DEFAULT NULL,
  `continent` varchar(3) NOT NULL,
  `isoCountry` varchar(3) NOT NULL,
  `isoRegion` varchar(10) NOT NULL,
  `municipality` varchar(150) NOT NULL,
  `scheduledService` varchar(5) NOT NULL,
  `gpsCode` varchar(6) NOT NULL,
  `iataCode` varchar(5) DEFAULT NULL,
  `localCode` varchar(6) DEFAULT NULL,
  `homeLink` varchar(150) DEFAULT NULL,
  `wikipediaLink` varchar(150) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Country`
--

DROP TABLE IF EXISTS `Country`;
CREATE TABLE `Country` (
  `id` varchar(6) CHARACTER SET utf8 NOT NULL,
  `code` varchar(2) CHARACTER SET utf8 NOT NULL,
  `name` varchar(80) CHARACTER SET utf8 NOT NULL,
  `continent` varchar(2) CHARACTER SET utf8 NOT NULL,
  `wikipediaLink` varchar(150) CHARACTER SET utf8 NOT NULL,
  `keywords` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Runway`
--

DROP TABLE IF EXISTS `Runway`;
CREATE TABLE `Runway` (
  `id` int(11) NOT NULL,
  `airportRef` bigint(20) NOT NULL,
  `airportIdentifier` varchar(10) NOT NULL,
  `lengthft` int(11) DEFAULT NULL,
  `widthFt` int(11) DEFAULT NULL,
  `surface` varchar(80) NOT NULL,
  `lighted` tinyint(4) NOT NULL,
  `closed` tinyint(4) NOT NULL,
  `leIdentifier` varchar(8) NOT NULL,
  `leLatitudeDeg` double DEFAULT NULL,
  `leLongitudeDeg` double DEFAULT NULL,
  `leElevationFt` int(11) DEFAULT NULL,
  `leHeadingDegT` double DEFAULT NULL,
  `leDisplacedThresholdFt` double DEFAULT NULL,
  `heIdentifier` varchar(10) NOT NULL,
  `heLatitudeDeg` double DEFAULT NULL,
  `heLongitudeDeg` double DEFAULT NULL,
  `heElevationFt` int(11) DEFAULT NULL,
  `heHeadingDegT` double DEFAULT NULL,
  `heDisplacedThresholdFt` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Airport`
--
ALTER TABLE `Airport`
  ADD PRIMARY KEY (`id`),
  ADD KEY `identifier` (`identifier`);

--
-- Indexes for table `Country`
--
ALTER TABLE `Country`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codeIdx` (`code`);

--
-- Indexes for table `Runway`
--
ALTER TABLE `Runway`
  ADD PRIMARY KEY (`id`),
  ADD KEY `airportRef` (`airportRef`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
